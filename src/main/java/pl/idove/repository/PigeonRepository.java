package pl.idove.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.idove.model.db.PigeonDb;

import java.util.List;

@Repository
public interface PigeonRepository extends JpaRepository<PigeonDb, Integer> {

    List<PigeonDb> getPigeonByIdFancier(int idFancier);
}
