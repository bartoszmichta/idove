package pl.idove.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.idove.model.db.CalculatorDb;

@Repository
public interface CalculatorRepository extends JpaRepository<CalculatorDb, Integer> {

    public CalculatorDb findByUserId(int userId);
}
