package pl.idove.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.idove.model.db.BranchDb;

import java.util.List;

public interface BranchRepository extends JpaRepository<BranchDb, Integer> {

    List<BranchDb> findByIdCalculator(int idCalculator);
}
