package pl.idove.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.idove.model.db.CountryDb;

@Repository
public interface CountryRepository extends JpaRepository<CountryDb, Integer> {

}
