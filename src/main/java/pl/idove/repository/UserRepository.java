package pl.idove.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import pl.idove.model.db.UserDb;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<UserDb, Integer> {


    @Query(value = "SELECT u.idUser, u.firstName, u.lastName, u.address, u.city, u.mail, u.telephoneNumber, f.sectionId, f.dovecoteId, f.fancierNumber, f.teamName " +
            "FROM t_user u \n" +
            "LEFT JOIN t_fancier f ON f.userId = u.idUser \n" +
            "LEFT JOIN t_section s ON s.idSection = f.sectionId \n" +
            "WHERE s.idBranch  = ?1", nativeQuery = true)
    List<Object> findUserByIdBranch(int idBranch);

    UserDb findByUsername(String username);

    UserDb findByEmail(String email);

    UserDb findByConfirmationToken(String confirmationToken);

    UserDb findByIdUser(int idUser);

}
