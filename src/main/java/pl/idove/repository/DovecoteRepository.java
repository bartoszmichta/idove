package pl.idove.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.idove.model.db.DovecoteDb;

@Repository
public interface DovecoteRepository extends JpaRepository<DovecoteDb, Integer> {

    public DovecoteDb findByIdDovecote(int idDovecote);

}
