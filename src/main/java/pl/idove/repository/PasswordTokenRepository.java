package pl.idove.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.idove.model.api.PasswordResetToken;

public interface PasswordTokenRepository extends JpaRepository<PasswordResetToken, Integer> {

    public PasswordResetToken findByToken(String token);
}
