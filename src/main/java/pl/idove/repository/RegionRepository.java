package pl.idove.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.idove.model.db.RegionDb;

@Repository
public interface RegionRepository extends JpaRepository<RegionDb, Integer> {
}
