package pl.idove.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.idove.model.db.FancierDb;

@Repository
public interface FancierRepository extends JpaRepository<FancierDb, Integer> {

    FancierDb findByUserId(int id);

}
