package pl.idove.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.idove.model.db.SectionDb;

import java.util.List;

@Repository
public interface SectionRepository extends JpaRepository<SectionDb, Integer> {

    List<SectionDb> findByIdBranch(int idBranch);

    SectionDb findByIdSection(int idSection);
}
