package pl.idove.helper;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;


public class HelperDate {

    public static String DateToUTCFormat(Date d) {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));

        return sdf.format(d);

    }


    public static Date YYYY_MM_DD_HH_MMToDate(String value) throws Exception {
        if (value.trim().length() == 0) {
            return null;
        } else {
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");

            return df.parse(value);
        }

    }

    public static String DateToYYYY_MM_DD(Date value) {
        String result = "";

        if (value != null) {

            DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            result = df.format(value);

        }

        return result;

    }

    public static String DateToHH_mm(Date value) {
        String result = "";

        if (value != null) {

            DateFormat df = new SimpleDateFormat("HH:mm");
            result = df.format(value);

        }

        return result;

    }

    public static String DateToYYYY_MM_DD_HH_MM(Date value) {
        String result = "";

        if (value != null) {

            DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            result = df.format(value);

        }

        return result;

    }

    public static String DateToYYYY_MM_DD_HH_MM_SS(Date value) {
        String result = "";

        if (value != null) {

            DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            result = df.format(value);

        }

        return result;

    }
}

