package pl.idove.model.db;

import javax.persistence.*;

@Entity
@Table(name = "t_pigeon")

@NamedStoredProcedureQueries({
        @NamedStoredProcedureQuery(
                name = "confirm_pigeon",
                procedureName = "sp_calculator_for_pigeon",
                parameters = {
                        @StoredProcedureParameter(name = "pigeonId", type = Integer.class, mode = ParameterMode.IN),
                        @StoredProcedureParameter(name = "userId", type = Integer.class, mode = ParameterMode.OUT)})
})


public class PigeonDb {

    @Id
    @Column(name = "idPigeon")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idPigeon;

    @Column(name = "idFancier")
    private int idFancier;

    @Column(name = "idCountry")
    private int idCountry;

    @Column(name = "idColor")
    private int idColor;

    @Column(name = "branchNumber")
    private String branchNumber;

    @Column(name = "yearbook")
    private String yearbook;

    @Column(name = "sex")
    private Boolean sex;

    @Column(name = "number")
    private String number;

    @Column(name = "ringNumber")
    private String ringNumber;

    @Column(name = "isGMP")
    private Boolean isGMP = false;

    @Column(name = "isMP")
    private Boolean isMP = false;

    @Column(name = "isIMP")
    private Boolean isIMP = false;

    @Column(name = "isConfirm")
    private Boolean isConfirm = false;

    public PigeonDb() {
    }

    public Boolean getConfirm() {
        return isConfirm;
    }

    public void setConfirm(Boolean confirm) {
        isConfirm = confirm;
    }

    public int getIdPigeon() {
        return idPigeon;
    }

    public void setIdPigeon(int idPigeon) {
        this.idPigeon = idPigeon;
    }

    public int getIdFancier() {
        return idFancier;
    }

    public void setIdFancier(int idFancier) {
        this.idFancier = idFancier;
    }

    public int getIdCountry() {
        return idCountry;
    }

    public void setIdCountry(int idCountry) {
        this.idCountry = idCountry;
    }

    public int getIdColor() {
        return idColor;
    }

    public void setIdColor(int idColor) {
        this.idColor = idColor;
    }

    public String getBranchNumber() {
        return branchNumber;
    }

    public void setBranchNumber(String branchNumber) {
        this.branchNumber = branchNumber;
    }

    public String getYearbook() {
        return yearbook;
    }

    public void setYearbook(String yearbook) {
        this.yearbook = yearbook;
    }

    public Boolean getSex() {
        return sex;
    }

    public void setSex(Boolean sex) {
        this.sex = sex;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getRingNumber() {
        return ringNumber;
    }

    public void setRingNumber(String ringNumber) {
        this.ringNumber = ringNumber;
    }

    public Boolean getGMP() {
        return isGMP;
    }

    public void setGMP(Boolean GMP) {
        isGMP = GMP;
    }

    public Boolean getMP() {
        return isMP;
    }

    public void setMP(Boolean MP) {
        isMP = MP;
    }

    public Boolean getIMP() {
        return isIMP;
    }

    public void setIMP(Boolean IMP) {
        isIMP = IMP;
    }
}
