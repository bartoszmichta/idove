package pl.idove.model.db;


import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "t_section")
public class SectionDb {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idSection")
    private Integer idSection;

    @Column(name = "idBranch")
    private Integer idBranch;

    @Column(name = "name")
    private String name;

    @OneToMany(mappedBy = "section", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Set<FancierDb> fancier;

    public SectionDb() {
    }

    public Integer getIdSection() {
        return idSection;
    }

    public void setIdSection(Integer idSection) {
        this.idSection = idSection;
    }

    public Integer getIdBranch() {
        return idBranch;
    }

    public void setIdBranch(Integer idBranch) {
        this.idBranch = idBranch;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

