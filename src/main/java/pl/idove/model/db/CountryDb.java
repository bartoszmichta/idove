package pl.idove.model.db;


import javax.persistence.*;

@Entity
@Table(name = "t_country")
public class CountryDb {

    @Id
    @Column(name = "idCountry")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idCountry;

    @Column(name = "name")
    private String name;

    @Column(name = "shortcut")
    private String shortcut;

    public CountryDb() {
    }

    public Integer getIdCountry() {
        return idCountry;
    }

    public void setIdCountry(Integer idCountry) {
        this.idCountry = idCountry;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getShortcut() {
        return shortcut;
    }

    public void setShortcut(String shortcut) {
        this.shortcut = shortcut;
    }
}

