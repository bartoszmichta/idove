package pl.idove.model.db;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;

@Entity
@Table(name = "t_fancier")

@NamedStoredProcedureQueries({
        @NamedStoredProcedureQuery(
                name = "add_fancier_to_section",
                procedureName = "sp_add_fancier_to_section",
                parameters = {
                        @StoredProcedureParameter(name = "userId", type = Integer.class, mode = ParameterMode.IN),
                        @StoredProcedureParameter(name = "sectionId", type = Integer.class, mode = ParameterMode.IN),
                        @StoredProcedureParameter(name = "fancierNumber", type = Integer.class, mode = ParameterMode.IN)})
})


public class FancierDb {

    @Id
    @Column(name = "idFancier")
    private Integer idFancier;

    @Column(name = "userId")
    private Integer userId;

    @Column(name = "sectionId")
    private Integer sectionId;

    @Column(name = "dovecoteId")
    private Integer dovecoteId;

    @Column(name = "fancierNumber")
    private Integer fancierNumber;

    @Column(name = "teamName")
    private String teamName;

    @Column(name = "isConfirm")
    private Boolean isConfirm;

    @NotFound(action = NotFoundAction.IGNORE)
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "sectionId", insertable = false, updatable = false)
    private SectionDb section;

    @OneToOne
    @JoinColumn(name = "userId", insertable = false, updatable = false)
    private UserDb user;

    public FancierDb() {
    }

    public Integer getIdFancier() {
        return idFancier;
    }

    public void setIdFancier(Integer idFancier) {
        this.idFancier = idFancier;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getSectionId() {
        return sectionId;
    }

    public void setSectionId(Integer sectionId) {
        this.sectionId = sectionId;
    }

    public Integer getDovecoteId() {
        return dovecoteId;
    }

    public void setDovecoteId(Integer dovecoteId) {
        this.dovecoteId = dovecoteId;
    }

    public Integer getFancierNumber() {
        return fancierNumber;
    }

    public void setFancierNumber(Integer fancierNumber) {
        this.fancierNumber = fancierNumber;
    }

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    public Boolean getConfirm() {
        return isConfirm;
    }

    public void setConfirm(Boolean confirm) {
        isConfirm = confirm;
    }

    public SectionDb getSection() {
        return section;
    }

    public void setSection(SectionDb section) {
        this.section = section;
    }

    public UserDb getUser() {
        return user;
    }

    public void setUser(UserDb user) {
        this.user = user;
    }
}
