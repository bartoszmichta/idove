package pl.idove.model.db;


import javax.persistence.*;


@Entity(name = "t_dovecote")

@NamedStoredProcedureQueries({
        @NamedStoredProcedureQuery(
                name = "add_dovecote",
                procedureName = "sp_add_dovecote",
                parameters = {
                        @StoredProcedureParameter(name = "userId", type = Integer.class, mode = ParameterMode.IN),
                        @StoredProcedureParameter(name = "address", type = String.class, mode = ParameterMode.IN),
                        @StoredProcedureParameter(name = "city", type = String.class, mode = ParameterMode.IN),
                        @StoredProcedureParameter(name = "latitude", type = String.class, mode = ParameterMode.IN),
                        @StoredProcedureParameter(name = "longitude", type = String.class, mode = ParameterMode.IN)}),

        @NamedStoredProcedureQuery(
                name = "getCalculatorByDovecote",
                procedureName = "sp_calculator_for_dovecote",
                parameters = {
                        @StoredProcedureParameter(name = "dovecoteId", type = Integer.class, mode = ParameterMode.IN),
                        @StoredProcedureParameter(name = "userId", type = Integer.class, mode = ParameterMode.OUT)})
})


public class DovecoteDb {

    @Id
    @Column(name = "idDovecote")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idDovecote;

    @Column(name = "address")
    private String address;

    @Column(name = "city")
    private String city;

    @Column(name = "latitude")
    private String latitude;

    @Column(name = "longitude")
    private String longitude;

    @Column(name = "isConfirm")
    private Boolean confirm;


    public int getIdDovecote() {
        return idDovecote;
    }

    public void setIdDovecote(int idDovecote) {
        this.idDovecote = idDovecote;
    }

    public String getAdrdress() {
        return address;
    }

    public void setAdrdress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongtitude() {
        return longitude;
    }

    public void setLongtitude(String longtitude) {
        this.longitude = longtitude;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public Boolean getConfirm() {
        return confirm;
    }

    public void setConfirm(Boolean confirm) {
        this.confirm = confirm;
    }
}
