package pl.idove.model.db;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "t_calculator")
public class CalculatorDb {

    @Id
    @Column(name = "idCalculator")
    private Integer idCalculator;

    @Column(name = "userId")
    private Integer userId;

    public CalculatorDb() {
    }

    public Integer getIdCalculator() {
        return idCalculator;
    }

    public void setIdCalculator(Integer idCalculator) {
        this.idCalculator = idCalculator;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }
}
