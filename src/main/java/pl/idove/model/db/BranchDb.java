package pl.idove.model.db;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "t_branch")
public class BranchDb {

    @Id
    @Column(name = "idBranch")
    private Integer idBranch;

    @Column(name = "branchNumber")
    private String branchNumber;

    @Column(name = "idRegion")
    private Integer idRegion;

    @Column(name = "name")
    private String name;

    @Column(name = "calculatorId")
    private Integer idCalculator;

    public BranchDb() {
    }

    public Integer getIdBranch() {
        return idBranch;
    }

    public void setIdBranch(Integer idBranch) {
        this.idBranch = idBranch;
    }

    public String getBranchNumber() {
        return branchNumber;
    }

    public void setBranchNumber(String branchNumber) {
        this.branchNumber = branchNumber;
    }

    public Integer getIdRegion() {
        return idRegion;
    }

    public void setIdRegion(Integer idRegion) {
        this.idRegion = idRegion;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getIdCalculator() {
        return idCalculator;
    }

    public void setIdCalculator(Integer idCalculator) {
        this.idCalculator = idCalculator;
    }
}
