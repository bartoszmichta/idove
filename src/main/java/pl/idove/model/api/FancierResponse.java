package pl.idove.model.api;


public class FancierResponse {

    private final Integer idUser;

    private final String firstName;

    private final String lastName;

    private final String address;

    private final String city;

    private final String mail;

    private final String telephoneNumber;

    private final Integer sectionId;

    private final Integer dovecoteId;

    private final Integer fancierNumber;

    private final String teamName;

    public FancierResponse(Integer idUser, String firstName, String lastName, String address, String city, String mail, String telephoneNumber, Integer sectionId, Integer dovecoteId, Integer fancierNumber, String teamName) {
        this.idUser = idUser;
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
        this.city = city;
        this.mail = mail;
        this.telephoneNumber = telephoneNumber;
        this.sectionId = sectionId;
        this.dovecoteId = dovecoteId;
        this.fancierNumber = fancierNumber;
        this.teamName = teamName;
    }

    public Integer getIdUser() {
        return idUser;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getAddress() {
        return address;
    }

    public String getCity() {
        return city;
    }

    public String getMail() {
        return mail;
    }

    public String getTelephoneNumber() {
        return telephoneNumber;
    }

    public Integer getSectionId() {
        return sectionId;
    }

    public Integer getDovecoteId() {
        return dovecoteId;
    }

    public Integer getFancierNumber() {
        return fancierNumber;
    }

    public String getTeamName() {
        return teamName;
    }
}
