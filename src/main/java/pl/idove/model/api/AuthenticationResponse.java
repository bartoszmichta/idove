package pl.idove.model.api;


import pl.idove.model.db.UserDb;

public class AuthenticationResponse {

    private AuthToken authToken;
    private UserDb userDb;

    public AuthenticationResponse(AuthToken authToken, UserDb userDb) {
        this.authToken = authToken;
        this.userDb = userDb;
        this.userDb.setPassword(null);
        this.userDb.setConfirmationToken(null);
    }

    public AuthToken getAuthToken() {
        return authToken;
    }

    public void setAuthToken(AuthToken authToken) {
        this.authToken = authToken;
    }

    public UserDb getUserDb() {
        return userDb;
    }

    public void setUserDb(UserDb userDb) {
        this.userDb = userDb;
    }
}
