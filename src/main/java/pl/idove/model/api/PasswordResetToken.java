package pl.idove.model.api;


import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "t_resetToken")
public class PasswordResetToken {

    private static final int EXPIRATION = 1000 * 60 * 60 * 24;

    @Id
    @Column(name = "idToken")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idToken;

    @Column(name = "token")
    private String token;

    @Column(name = "userId")
    private Integer userId;

    @Column(name = "expiryDate")
    private Date expiryDate;


    public PasswordResetToken() {
    }

    public PasswordResetToken(String token, Integer userId) {
        this.token = token;
        this.userId = userId;
        Date now = new Date();
        this.expiryDate = new Date(now.getTime() + EXPIRATION);
    }

    public Integer getIdToken() {
        return idToken;
    }

    public void setIdToken(Integer idToken) {
        this.idToken = idToken;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Date getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(Date expiryDate) {
        this.expiryDate = expiryDate;
    }
}

