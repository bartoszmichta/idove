package pl.idove.model.api;

public class FancierRequest {

    private int userId;

    private int sectionId;

    private int fancierNumber;

    public FancierRequest() {
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getSectionId() {
        return sectionId;
    }

    public void setSectionId(int sectionId) {
        this.sectionId = sectionId;
    }

    public int getFancierNumber() {
        return fancierNumber;
    }

    public void setFancierNumber(int fancierNumber) {
        this.fancierNumber = fancierNumber;
    }
}
