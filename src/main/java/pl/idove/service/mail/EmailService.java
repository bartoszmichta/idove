package pl.idove.service.mail;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import pl.idove.model.db.UserDb;
import pl.idove.service.db.UserService;

import javax.servlet.http.HttpServletRequest;

@Service
public class EmailService {

    private JavaMailSender mailSender;
    private UserService userService;

    @Autowired
    public EmailService(JavaMailSender mailSender, UserService userService) {

        this.mailSender = mailSender;
        this.userService = userService;
    }

    @Async
    public void sendRegisterEmail(UserDb user, HttpServletRequest request) {

        String appUrl = "http://" + request.getServerName() + ":4200";
        SimpleMailMessage registrationEmail = new SimpleMailMessage();
        registrationEmail.setTo(user.getEmail());
        registrationEmail.setSubject("Potwierdzenie rejestracji");
        registrationEmail.setText("W celu potwierdzenia adresu email proszę kliknąć link poniżej:\n"
                + appUrl + "/confirm?token=" + user.getConfirmationToken());
        registrationEmail.setFrom("idove.info@gmail.com");
        mailSender.send(registrationEmail);
    }

    @Async
    public void sendReminderEmail(UserDb user, HttpServletRequest request, String token) {

        String appUrl = "http://" + request.getServerName() + ":4200";
        SimpleMailMessage reminderEmail = new SimpleMailMessage();
        reminderEmail.setTo(user.getEmail());
        reminderEmail.setSubject("Zmiana hasła do konta IDove");
        reminderEmail.setText("Witaj " + user.getUsername() + "\n\n"
                + "Ktoś rozpoczął odzyskiwanie hasła, jeżeli to nie byłeś Ty, to zignoruj tę wiadomość.\n" +
                "Jeżeli jednak chcesz odzyskać hasło do swojego konta " + user.getEmail() + ", to przejdź do tej strony \n"
                + appUrl + "/change-password?token=" + token);
        reminderEmail.setFrom("idove.info@gmail.com");
        mailSender.send(reminderEmail);
    }
}
