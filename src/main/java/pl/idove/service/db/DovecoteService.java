package pl.idove.service.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.idove.model.db.DovecoteDb;
import pl.idove.repository.DovecoteRepository;
import pl.idove.utils.ExceptionInfo;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.StoredProcedureQuery;

@Service
public class DovecoteService {

    private DovecoteRepository dovecoteRepository;

    private UserService userService;

    private EntityManager entityManager;

    @Autowired
    public DovecoteService(DovecoteRepository dovecoteRepository, UserService userService, EntityManager entityManager) {
        this.dovecoteRepository = dovecoteRepository;
        this.userService = userService;
        this.entityManager = entityManager;
    }

    public DovecoteDb getDovecoteById(int idDovecote) {


        return dovecoteRepository.findByIdDovecote(idDovecote);
    }

    public void addDovecote(DovecoteDb dovecoteDb) {

        StoredProcedureQuery addFancier = entityManager.createNamedStoredProcedureQuery("add_dovecote");
        addFancier.setParameter("userId", userService.getAuthenticatedUserId());
        addFancier.setParameter("address", dovecoteDb.getAdrdress());
        addFancier.setParameter("city", dovecoteDb.getCity());
        addFancier.setParameter("latitude", dovecoteDb.getLatitude());
        addFancier.setParameter("longitude", dovecoteDb.getLongtitude());
        addFancier.execute();

    }

    public void modifyDovecote(DovecoteDb dovecote, int dovecoteId) {

        DovecoteDb dovecoteDb = dovecoteRepository.findByIdDovecote(dovecoteId);
        dovecoteDb.setAdrdress(dovecote.getAdrdress());
        dovecoteDb.setCity(dovecote.getCity());
        dovecoteDb.setLatitude(dovecote.getLatitude());
        dovecoteDb.setLongtitude(dovecote.getLongtitude());
        dovecoteRepository.save(dovecoteDb);
    }

    public ExceptionInfo confirmDovecote(int idDovecote) {

        StoredProcedureQuery query = entityManager.createNamedStoredProcedureQuery("getCalculatorByDovecote");
        query.setParameter("dovecoteId", idDovecote);
        query.execute();
        Integer userId = (Integer) query.getOutputParameterValue("userId");

        if (userService.getAuthenticatedUserId() != userId) {

            return ExceptionInfo.FORRBIDEN_ACCES;
        } else {

            DovecoteDb dovecoteDb = dovecoteRepository.findByIdDovecote(idDovecote);
            dovecoteDb.setConfirm(true);
            dovecoteRepository.save(dovecoteDb);
            return ExceptionInfo.OK;
        }
    }

}
