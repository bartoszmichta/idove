package pl.idove.service.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.idove.model.db.DovecoteDb;
import pl.idove.model.db.PigeonDb;
import pl.idove.repository.PigeonRepository;
import pl.idove.utils.ExceptionInfo;

import javax.persistence.EntityManager;
import javax.persistence.StoredProcedureQuery;
import java.util.List;
import java.util.Optional;

@Service
public class PigeonService {

    private PigeonRepository pigeonRepository;

    private UserService userService;

    private FancierService fancierService;

    private EntityManager entityManager;

    @Autowired
    public PigeonService(PigeonRepository pigeonRepository, UserService userService, FancierService fancierService, EntityManager entityManager) {
        this.pigeonRepository = pigeonRepository;
        this.userService = userService;
        this.fancierService = fancierService;
        this.entityManager = entityManager;
    }

    public List<PigeonDb> getPigeonByFancierId(int idFancier) {
        return pigeonRepository.getPigeonByIdFancier(idFancier);
    }

    public Optional<PigeonDb> getPigeon(int id) {

        Optional<PigeonDb> pigeon = pigeonRepository.findById(id);

        if (pigeon.isPresent()) {

            Integer userId = pigeon.get().getIdFancier();
            Integer authenticatedUserId = fancierService.getFancierIdByUserId(userService.getAuthenticatedUserId());

            if (userId == authenticatedUserId) {

                return pigeon;

            }
        } else return pigeon;


        return pigeonRepository.findById(id);
    }

    public ExceptionInfo deletePigeon(int id) {

        Optional<PigeonDb> pigeonDb = getPigeon(id);

        if (pigeonDb.isPresent()) {

            if (pigeonDb.get().getIdFancier() == fancierService.
                    getFancierIdByUserId(userService.getAuthenticatedUserId())) {

                pigeonRepository.deleteById(id);
                return ExceptionInfo.OK;
            } else {

                return ExceptionInfo.RESOURCE_NOT_FOUND;
            }
        } else {

            return ExceptionInfo.RESOURCE_NOT_FOUND;
        }
    }

    public void addPigeon(PigeonDb pigeonDb) {

        pigeonDb.setIdFancier(fancierService.getFancierIdByUserId(userService.getAuthenticatedUserId()));
        pigeonRepository.save(pigeonDb);
    }

    public ExceptionInfo modifyPigeon(PigeonDb pigeonDb, int id) {

        Optional<PigeonDb> pigeon = pigeonRepository.findById(id);
        if (pigeon.isPresent()) {

            Integer fancierId = pigeon.get().getIdFancier();
            Integer authenticatedUserId = fancierService.getFancierIdByUserId(userService.getAuthenticatedUserId());

            if (fancierId == authenticatedUserId) {

                pigeon.get().setIdCountry(pigeonDb.getIdCountry());
                pigeon.get().setIdColor(pigeonDb.getIdColor());
                pigeon.get().setBranchNumber(pigeonDb.getBranchNumber());
                pigeon.get().setYearbook(pigeonDb.getYearbook());
                pigeon.get().setGMP(pigeonDb.getMP());
                pigeon.get().setMP(pigeonDb.getMP());
                pigeon.get().setIMP(pigeonDb.getIMP());
                pigeon.get().setSex(pigeonDb.getSex());
                pigeon.get().setNumber(pigeonDb.getNumber());
                addPigeon(pigeon.get());

            } else {

                return ExceptionInfo.FORRBIDEN_ACCES;

            }
        } else {

            return ExceptionInfo.RESOURCE_NOT_FOUND;

        }

        return ExceptionInfo.OK;
    }

    public ExceptionInfo confirmPigeon(int pigeonId) {

        StoredProcedureQuery query = entityManager.createNamedStoredProcedureQuery("confirm_pigeon");
        query.setParameter("pigeonId", pigeonId);
        query.execute();
        Integer userId = (Integer) query.getOutputParameterValue("userId");

        if (userService.getAuthenticatedUserId() != userId) {

            return ExceptionInfo.FORRBIDEN_ACCES;
        } else {

            PigeonDb pigeonDb = pigeonRepository.getOne(pigeonId);
            pigeonDb.setConfirm(true);
            pigeonRepository.save(pigeonDb);
            return ExceptionInfo.OK;
        }

    }
}




