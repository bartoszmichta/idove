package pl.idove.service.db;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.idove.model.db.FancierDb;
import pl.idove.repository.FancierRepository;
import pl.idove.utils.ExceptionInfo;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.StoredProcedureQuery;
import java.util.List;

@Service
public class FancierService {

    private final EntityManager entityManager;

    private FancierRepository fancierRepository;

    private UserService userService;

    private SectionService sectionService;

    @Autowired
    public FancierService(EntityManager entityManager, FancierRepository fancierRepository, UserService userService, SectionService sectionService) {
        this.entityManager = entityManager;
        this.fancierRepository = fancierRepository;
        this.userService = userService;
        this.sectionService = sectionService;
    }

    public int getFancierIdByUserId(int userId) {

        FancierDb fancierDb = fancierRepository.findByUserId(userId);
        fancierDb.setSection(null);
        fancierDb.setUser(null);
        return fancierDb.getIdFancier();

    }

    public FancierDb getFancierByUserId(int userId) {

        FancierDb fancierDb = fancierRepository.findByUserId(userId);
        fancierDb.setSection(null);
        fancierDb.setUser(null);
        return fancierDb;

    }

    public ExceptionInfo addFancierToSection(int userId, int sectionId, int fancierNumber) {

        if (userService.findByUserId(userId) == null || sectionService.findBySectionId(sectionId) == null) {

            return ExceptionInfo.RESOURCE_NOT_FOUND;

        } else {

            StoredProcedureQuery addFancier = entityManager.createNamedStoredProcedureQuery("add_fancier_to_section");
            addFancier.setParameter("userId", userId);
            addFancier.setParameter("sectionId", sectionId);
            addFancier.setParameter("fancierNumber", fancierNumber);
            addFancier.execute();
            return ExceptionInfo.OK;
        }
    }

    public List<FancierDb> getFancierByBranchId(int idBranch) {

        Query q = entityManager.createNativeQuery("SELECT * FROM t_user u LEFT JOIN t_fancier f ON  f.userId = u.idUser\n" +
                "LEFT JOIN t_section s ON s.idSection = f.sectionId WHERE s.idBranch = ? ", FancierDb.class);
        q.setParameter(1, idBranch);

        List<FancierDb> fancier = q.getResultList();
        for (FancierDb item : fancier) {

            item.getUser().setPassword(null);
            item.getUser().setConfirmationToken(null);
            item.getUser().setRegistrationDate(null);
            item.getUser().setActive(null);
            item.getUser().setCalculator(null);
            item.getUser().setFancier(null);

        }

        return fancier;
    }

}
