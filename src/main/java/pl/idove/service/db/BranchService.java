package pl.idove.service.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.idove.model.db.BranchDb;
import pl.idove.model.db.UserDb;
import pl.idove.repository.BranchRepository;

import java.util.List;

@Service
public class BranchService {

    private BranchRepository branchRepository;

    private CalculatorService calculatorService;

    private UserService userService;


    @Autowired
    public BranchService(BranchRepository branchRepository, CalculatorService calculatorService, UserService userService) {
        this.branchRepository = branchRepository;
        this.calculatorService = calculatorService;
        this.userService = userService;
    }

    public List<BranchDb> getAllBranches() {

        return branchRepository.findAll();

    }

    public List<BranchDb> getBranchesByCalculatorId() {

        UserDb userDb = userService.findByUserId();
        if (userDb.getCalculator()) {
            return branchRepository.findByIdCalculator(calculatorService.getCalculatorIdByUserId());
        } else {

            return null;
        }


    }
}
