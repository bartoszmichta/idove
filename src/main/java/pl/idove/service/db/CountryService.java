package pl.idove.service.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.idove.model.db.CountryDb;
import pl.idove.repository.CountryRepository;

import java.util.List;

@Service
public class CountryService {

    private CountryRepository countryRepository;

    @Autowired
    public CountryService(CountryRepository countryRepository) {
        this.countryRepository = countryRepository;
    }

    public List<CountryDb> getAllCountries() {

        return countryRepository.findAll();
    }

    public void addCountry(CountryDb countryDb) {

        countryRepository.save(countryDb);
    }
}
