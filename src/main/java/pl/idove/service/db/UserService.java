package pl.idove.service.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import pl.idove.helper.HelperDate;
import pl.idove.model.db.UserDb;
import pl.idove.repository.UserRepository;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;


@Service(value = "userService")
public class UserService implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private PasswordEncoder passwordEncoder;

    private EntityManager entityManager;

    public UserService(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserDb userDb = userRepository.findByUsername(username);
        if (userDb == null) {
            throw new UsernameNotFoundException("Invalid username or password.");
        }
        return new org.springframework.security.core.userdetails.User(userDb.getUsername(), userDb.getPassword(), getAuthorities());
    }

    private List<GrantedAuthority> getAuthorities() {
        List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();

        authorities.add(new SimpleGrantedAuthority("ADMIN"));
        return authorities;
    }

    public UserDb findByUsername(String username) throws UsernameNotFoundException {
        return userRepository.findByUsername(username);
    }

    public UserDb findByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    public UserDb findByConfirmationToken(String confirmationToken) {
        return userRepository.findByConfirmationToken(confirmationToken);
    }

    public UserDb findByUserId(int userId) {

        return userRepository.findByIdUser(userId);

    }

    public UserDb findByUserId() {

        return userRepository.findByIdUser(getAuthenticatedUserId());

    }

    public void setLastLoginDate() {

        UserDb userDb = findByUserId();
        Date date = new Date();
        userDb.setLastLogin(HelperDate.DateToYYYY_MM_DD_HH_MM_SS(date));
        saveUser(userDb);

    }

    public void saveUser(UserDb user) {

        userRepository.save(user);
    }

    public void registerUser(UserDb user) {

        user.setActive(false);
        Date date = new Date();
        user.setRegistrationDate(HelperDate.DateToYYYY_MM_DD_HH_MM(date));
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        user.setConfirmationToken(UUID.randomUUID().toString());
        saveUser(user);
    }

    public Integer getAuthenticatedUserId() {

        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String username = user.getUsername();
        UserDb userDb = this.findByUsername(username);
        return userDb.getIdUser();

    }

    public UserDb modifyUserDetails(UserDb user) {

        UserDb userDb = findByUserId();

        userDb.setEmail(user.getEmail());
        userDb.setFirstName(user.getFirstName());
        userDb.setLastName(user.getLastName());
        userDb.setAddress(user.getAddress());
        userDb.setCity(user.getCity());
        userDb.setTelephoneNumber(user.getTelephoneNumber());
        userRepository.save(userDb);

        return userDb;

    }

    public List<UserDb> getUsersByEmail(String mail) {

        Query q = entityManager.createNativeQuery("SELECT * FROM t_user  WHERE mail LIKE ?1", UserDb.class);
        q.setParameter(1, "%" + mail + "%");
        List<UserDb> user_item = q.getResultList();

        for (UserDb user : user_item) {

            user.setPassword(null);
            user.setConfirmationToken(null);
            user.setRegistrationDate(null);
            user.setLastLogin(null);

        }

        return user_item;
    }

}
