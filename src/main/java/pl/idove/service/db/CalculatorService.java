package pl.idove.service.db;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.idove.model.db.CalculatorDb;
import pl.idove.repository.CalculatorRepository;

@Service
public class CalculatorService {

    private CalculatorRepository calculatorRepository;

    private UserService userService;

    @Autowired
    public CalculatorService(CalculatorRepository calculatorRepository, UserService userService) {
        this.calculatorRepository = calculatorRepository;
        this.userService = userService;
    }

    public int getCalculatorIdByUserId() {

        CalculatorDb calculatorDb = calculatorRepository.findByUserId(userService.getAuthenticatedUserId());
        return calculatorDb.getIdCalculator();

    }
}
