package pl.idove.service.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.idove.model.db.SectionDb;
import pl.idove.repository.SectionRepository;

import java.util.List;

@Service
public class SectionService {

    private SectionRepository sectionRepository;

    @Autowired
    public SectionService(SectionRepository sectionRepository) {
        this.sectionRepository = sectionRepository;
    }

    public List<SectionDb> getSectionByIdBranch(int idBranch) {

        return sectionRepository.findByIdBranch(idBranch);
    }

    public void addSection(SectionDb sectionDb) {

        sectionRepository.save(sectionDb);
    }

    public SectionDb findBySectionId(int idSection) {

        return sectionRepository.findByIdSection(idSection);
    }
}
