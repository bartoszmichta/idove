package pl.idove.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import pl.idove.model.api.PasswordResetToken;
import pl.idove.model.db.UserDb;
import pl.idove.repository.PasswordTokenRepository;
import pl.idove.service.db.UserService;
import pl.idove.service.mail.EmailService;
import pl.idove.utils.ExceptionInfo;

import javax.servlet.http.HttpServletRequest;
import java.util.Calendar;
import java.util.UUID;

@Service
public class PasswordService {

    @Autowired
    private UserService userService;

    @Autowired
    private PasswordTokenRepository passwordRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private EmailService emailService;

    public PasswordService(UserService userService, PasswordTokenRepository passwordRepository, EmailService emailService) {
        this.userService = userService;
        this.passwordRepository = passwordRepository;
        this.emailService = emailService;
    }

    public void addPasswordResetToken(UserDb userDb, String token) {

        PasswordResetToken resetToken = new PasswordResetToken(token, userDb.getIdUser());
        passwordRepository.save(resetToken);

    }

    public ExceptionInfo changePassword(String oldPassword, String newPassword) {

        UserDb userDb = userService.findByUserId();

        if (passwordEncoder.matches(oldPassword, userDb.getPassword())) {

            userDb.setPassword(passwordEncoder.encode(newPassword));
            userService.saveUser(userDb);
            return ExceptionInfo.PASSWORD_CHANGED;

        } else {

            return ExceptionInfo.PASSWORD_DO_NOT_MATCH;

        }
    }

    public ExceptionInfo passwordReset(HttpServletRequest request, String email) {

        UserDb userDb = userService.findByEmail(email);
        if (userDb == null) {

            return ExceptionInfo.RESOURCE_NOT_FOUND;

        } else {

            String token = UUID.randomUUID().toString();
            addPasswordResetToken(userDb, token);
            userDb.setPassword("");
            userService.saveUser(userDb);
            emailService.sendReminderEmail(userDb, request, token);
            return ExceptionInfo.RESET_TOKEN_SEND;
        }
    }

    public ExceptionInfo setNewPassword(String newPassword, String token) {

        PasswordResetToken passToken = passwordRepository.findByToken(token);
        if (passToken == null || passToken.getUserId() == null) {

            return ExceptionInfo.INVALID_TOKEN;
        } else {

            Calendar cal = Calendar.getInstance();
            if ((passToken.getExpiryDate().getTime() - cal.getTime().getTime()) <= 0) {

                return ExceptionInfo.TOKEN_EXPIRED;

            } else {

                UserDb userDb = userService.findByUserId(passToken.getUserId());
                userDb.setPassword(passwordEncoder.encode(newPassword));
                userService.saveUser(userDb);
                return ExceptionInfo.PASSWORD_CHANGED;
            }
        }
    }
}
