package pl.idove.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import pl.idove.config.JwtTokenUtil;
import pl.idove.model.api.AuthToken;
import pl.idove.model.api.AuthenticationResponse;
import pl.idove.model.db.UserDb;
import pl.idove.service.db.UserService;
import pl.idove.utils.ExceptionInfo;
import pl.idove.utils.ExceptionJSONInfo;

@CrossOrigin(origins = "localhost:4200", maxAge = 3600)
@RestController
public class AuthenticationController {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Autowired
    private UserService userService;

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public ResponseEntity<?> login(@RequestBody UserDb loginUser) throws AuthenticationException {

        final Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        loginUser.getUsername(),
                        loginUser.getPassword()
                )
        );
        SecurityContextHolder.getContext().setAuthentication(authentication);
        final UserDb user = userService.findByUsername(loginUser.getUsername());

        if (user.getActive() == Boolean.FALSE) {

            ExceptionJSONInfo exceptionJSONInfo = new ExceptionJSONInfo(HttpStatus.FORBIDDEN, ExceptionInfo.INACTIVE_ACCOUNT, "/login");
            return new ResponseEntity(exceptionJSONInfo, exceptionJSONInfo.getStatus());

        } else {

            userService.setLastLoginDate();
            final String token = jwtTokenUtil.generateToken(user);
            return ResponseEntity.ok(new AuthenticationResponse(new AuthToken(token), user));
        }

    }
}
