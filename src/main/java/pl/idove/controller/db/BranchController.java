package pl.idove.controller.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.idove.model.db.BranchDb;
import pl.idove.service.db.BranchService;

import java.util.List;

@RestController
@RequestMapping("/branch")
public class BranchController {

    private BranchService branchService;

    @Autowired
    public BranchController(BranchService branchService) {
        this.branchService = branchService;
    }

    @GetMapping
    public List<BranchDb> getAllBranches() {

        return branchService.getAllBranches();

    }

    @GetMapping(value = "/calculator")
    @SuppressWarnings("unchecked")
    public ResponseEntity<List<BranchDb>> getBranchesByCalculatorId() {

        if (branchService.getBranchesByCalculatorId() != null) {

            return new ResponseEntity(branchService.getBranchesByCalculatorId(), HttpStatus.OK);
        } else {

            return new ResponseEntity(HttpStatus.FORBIDDEN);
        }
    }
}
