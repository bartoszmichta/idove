package pl.idove.controller.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.idove.model.db.UserDb;
import pl.idove.service.db.UserService;
import pl.idove.service.mail.EmailService;
import pl.idove.utils.ExceptionInfo;
import pl.idove.utils.ExceptionJSONInfo;

import javax.servlet.http.HttpServletRequest;

@RestController
public class RegisterController {

    private UserService userService;
    private EmailService emailService;

    @Autowired
    public RegisterController(UserService userService, EmailService emailService) {
        this.userService = userService;
        this.emailService = emailService;
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public ResponseEntity<Object> processRegistration(@RequestBody UserDb user, HttpServletRequest request) {

        UserDb userExists = userService.findByEmail(user.getEmail());

        if (userExists != null) {
            ExceptionJSONInfo exceptionJSONInfo = new ExceptionJSONInfo(HttpStatus.CONFLICT, ExceptionInfo.USER_ALREADY_EXIST, "/register");
            return new ResponseEntity(exceptionJSONInfo, exceptionJSONInfo.getStatus());

        } else {

            userService.registerUser(user);
            emailService.sendRegisterEmail(user, request);
            return new ResponseEntity(HttpStatus.OK);
        }

    }


    @RequestMapping(value = "/confirm", method = RequestMethod.GET)
    public ResponseEntity<Object> processConfirmation(@RequestParam String confirmationToken) {

        UserDb user = userService.findByConfirmationToken(confirmationToken);
        if (user == null) {

            ExceptionJSONInfo exceptionJSONInfo = new ExceptionJSONInfo(HttpStatus.NOT_FOUND, ExceptionInfo.RESOURCE_NOT_FOUND, "/register");
            return new ResponseEntity(exceptionJSONInfo, exceptionJSONInfo.getStatus());

        } else {
            if (user.getActive() == Boolean.TRUE) {

                ExceptionJSONInfo exceptionJSONInfo = new ExceptionJSONInfo(HttpStatus.FORBIDDEN, ExceptionInfo.ACCOUNT_IS_ACTIVE, "/register");
                return new ResponseEntity(exceptionJSONInfo, exceptionJSONInfo.getStatus());

            } else {
                user.setActive(true);
                userService.saveUser(user);
                return new ResponseEntity(HttpStatus.OK);

            }

        }

    }
}

