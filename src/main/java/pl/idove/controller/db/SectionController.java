package pl.idove.controller.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.idove.model.db.SectionDb;
import pl.idove.service.db.SectionService;

import java.util.List;

@RestController
@RequestMapping("/section")
public class SectionController {

    private SectionService sectionService;

    @Autowired
    public SectionController(SectionService sectionService) {
        this.sectionService = sectionService;
    }

    @GetMapping
    public List<SectionDb> getSectionByIdBranch(@RequestParam int idBranch) {

        return sectionService.getSectionByIdBranch(idBranch);

    }

    @PostMapping
    public ResponseEntity addSection(@RequestBody SectionDb sectionDb) {

        sectionService.addSection(sectionDb);
        return new ResponseEntity(HttpStatus.CREATED);

    }
}
