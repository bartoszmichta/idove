package pl.idove.controller.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.idove.model.db.DovecoteDb;
import pl.idove.service.db.DovecoteService;
import pl.idove.utils.ExceptionInfo;

import javax.persistence.EntityManager;

@RestController
@RequestMapping(value = "/dovecote")
public class DovecoteController {

    private DovecoteService dovecoteService;

    @Autowired
    public DovecoteController(DovecoteService dovecoteService) {
        this.dovecoteService = dovecoteService;
    }

    @GetMapping
    public DovecoteDb getDovecoteById(@RequestParam int idDovecote) {

        return dovecoteService.getDovecoteById(idDovecote);
    }

    @PutMapping
    public void modifyDovecote(@RequestBody DovecoteDb dovecoteDb, @RequestParam int dovecoteId) {

        dovecoteService.modifyDovecote(dovecoteDb, dovecoteId);

    }

    @PostMapping
    public void addDovecote(@RequestBody DovecoteDb dovecoteDb) {

        dovecoteService.addDovecote(dovecoteDb);
    }

    @PutMapping(value = "/confirm")
    public ExceptionInfo confirmDovecote(@RequestParam int idDovecote) {

        return dovecoteService.confirmDovecote(idDovecote);
    }
}
