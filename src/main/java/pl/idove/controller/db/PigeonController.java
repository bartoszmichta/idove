package pl.idove.controller.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.idove.model.db.PigeonDb;
import pl.idove.service.db.FancierService;
import pl.idove.service.db.PigeonService;
import pl.idove.service.db.UserService;
import pl.idove.utils.ExceptionInfo;
import pl.idove.utils.ExceptionJSONInfo;

import java.util.Optional;

@RestController
@RequestMapping("/pigeon")
public class PigeonController {

    private PigeonService pigeonService;

    private UserService userService;

    private FancierService fancierService;

    @Autowired
    public PigeonController(PigeonService pigeonService, UserService userService, FancierService fancierService) {
        this.pigeonService = pigeonService;
        this.userService = userService;
        this.fancierService = fancierService;
    }

    @GetMapping
    @SuppressWarnings("unchecked")
    public ResponseEntity getPigeonsByFancierId() {

        return new ResponseEntity(pigeonService.getPigeonByFancierId(fancierService.
                getFancierIdByUserId(userService.getAuthenticatedUserId())), HttpStatus.OK);

    }

    @GetMapping(params = "fancierId")
    public ResponseEntity getPigeonsByFancierId(@RequestParam int fancierId) {

        return new ResponseEntity(pigeonService.getPigeonByFancierId(fancierId), HttpStatus.OK);
    }

    @GetMapping(params = "pigeonId")
    public ResponseEntity<Object> getPigeonById(@RequestParam int pigeonId) {

        Optional<PigeonDb> pigeon = pigeonService.getPigeon(pigeonId);
        if (!pigeon.isPresent()) {

            return new ResponseEntity(HttpStatus.NOT_FOUND);

        } else {

            return new ResponseEntity(pigeon, HttpStatus.OK);

        }


    }

    @DeleteMapping(params = "pigeonId")
    public ExceptionInfo deletePigeon(@RequestParam int pigeonId) {

        return pigeonService.deletePigeon(pigeonId);

    }

    @PostMapping
    public ResponseEntity addPigeon(@RequestBody PigeonDb pigeonDb) {
        pigeonService.addPigeon(pigeonDb);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @PutMapping(params = "pigeonId")
    public ExceptionJSONInfo modifyPigeon(@RequestBody PigeonDb pigeonDb, @RequestParam int pigeonId) {

        return new ExceptionJSONInfo(pigeonService.modifyPigeon(pigeonDb, pigeonId), "/pigeon");
    }

    @PutMapping(value = "/confirm")
    public ExceptionInfo confirmPigeon(@RequestParam int pigeonId) {

        return pigeonService.confirmPigeon(pigeonId);

    }
}

