package pl.idove.controller.db;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.idove.model.api.FancierRequest;
import pl.idove.model.db.FancierDb;
import pl.idove.service.db.FancierService;
import pl.idove.service.db.UserService;
import pl.idove.utils.ExceptionInfo;

import javax.persistence.Tuple;
import java.util.List;

@RestController
@RequestMapping("/fancier")
public class FancierController {


    private FancierService fancierService;

    private UserService userService;

    @Autowired
    public FancierController(FancierService fancierService, UserService userService) {
        this.fancierService = fancierService;
        this.userService = userService;
    }

    @GetMapping
    @SuppressWarnings("unchecked")
    public ResponseEntity<FancierDb> getFancierByUserId() {

        return new ResponseEntity(fancierService.getFancierByUserId(userService.getAuthenticatedUserId()), HttpStatus.OK);

    }

    @GetMapping(value = "/branch")
    public List<FancierDb> getFanciersByIdBranch(@RequestParam int idBranch) {

        return fancierService.getFancierByBranchId(idBranch);
    }

    @PostMapping()
    public ExceptionInfo addFancierToSection(@RequestBody FancierRequest fancierRequest) {

        return fancierService.addFancierToSection(fancierRequest.getUserId(),
                fancierRequest.getSectionId(), fancierRequest.getFancierNumber());

    }
}
