package pl.idove.controller.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.idove.model.db.CountryDb;
import pl.idove.service.db.CountryService;

import java.util.List;

@RestController
@RequestMapping("/country")
public class CountryController {

    private CountryService countryService;

    @Autowired
    public CountryController(CountryService countryService) {
        this.countryService = countryService;
    }

    @GetMapping
    public List<CountryDb> getAllCountries() {

        return countryService.getAllCountries();
    }

    @PostMapping
    public void addCountry(@RequestBody CountryDb countryDb) {

        countryService.addCountry(countryDb);

    }


}
