package pl.idove.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.idove.model.api.PasswordChangeRequest;
import pl.idove.service.PasswordService;
import pl.idove.utils.ExceptionInfo;

import javax.servlet.http.HttpServletRequest;

@RestController
public class PasswordController {

    @Autowired
    private PasswordService passwordService;


    @PutMapping(value = "/user/change-password")
    public ExceptionInfo changePassword(@RequestBody PasswordChangeRequest password) {

        return passwordService.changePassword(password.getOldPassword(), password.getNewPassword());

    }

    @RequestMapping(value = "/reset-password", method = RequestMethod.GET)
    public ExceptionInfo resetPassword(HttpServletRequest request, @RequestParam String userEmail) {

        return passwordService.passwordReset(request, userEmail);

    }

    @PutMapping(value = "/change-password")
    public ExceptionInfo setNewPassword(@RequestBody PasswordChangeRequest password, @RequestParam String token) {

        return passwordService.setNewPassword(password.getNewPassword(), token);

    }


}
