package pl.idove.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.*;
import pl.idove.model.db.UserDb;
import pl.idove.service.db.UserService;

import java.util.List;

@RestController
@RequestMapping("/user")
@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
public class UserController {

    @Autowired
    private UserService userService;


    @GetMapping
    public UserDb getUserByUserId() {

        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String username = user.getUsername();
        UserDb userDb = userService.findByUsername(username);
        userDb.setPassword(null);
        userDb.setConfirmationToken(null);
        return userDb;
    }

    @PutMapping
    public ResponseEntity modifyUserByUserId(@RequestBody UserDb user) {

        userService.modifyUserDetails(user);
        return new ResponseEntity(HttpStatus.OK);

    }

    @GetMapping(value = "/section")

    public @ResponseBody
    List<UserDb> getUsersByEmail(@RequestParam String email) {

        return userService.getUsersByEmail(email);

    }
}
