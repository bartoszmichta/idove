package pl.idove.utils;

import org.springframework.http.HttpStatus;

import java.util.Date;

public class ExceptionJSONInfo {

    private Date timestamp;
    private HttpStatus status;
    private ExceptionInfo exceptionInfo;
    private String errors;
    private String path;

    public ExceptionJSONInfo(HttpStatus status, ExceptionInfo exceptionInfo, String errors, String path) {
        this.timestamp = new Date();
        this.status = status;
        this.exceptionInfo = exceptionInfo;
        this.errors = errors;
        this.path = path;
    }

    public ExceptionJSONInfo(HttpStatus status, ExceptionInfo exceptionInfo, String path) {
        this.timestamp = new Date();
        this.status = status;
        this.exceptionInfo = exceptionInfo;
        this.path = path;
    }

    public ExceptionJSONInfo(ExceptionInfo exceptionInfo, String path) {
        this.timestamp = new Date();
        this.exceptionInfo = exceptionInfo;
        this.path = path;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public HttpStatus getStatus() {
        return status;
    }

    public void setStatus(HttpStatus status) {
        this.status = status;
    }

    public ExceptionInfo getExceptionInfo() {
        return exceptionInfo;
    }

    public void setExceptionInfo(ExceptionInfo exceptionInfo) {
        this.exceptionInfo = exceptionInfo;
    }

    public String getErrors() {
        return errors;
    }

    public void setErrors(String errors) {
        this.errors = errors;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
